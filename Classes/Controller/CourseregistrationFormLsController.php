<?php

namespace DRK\DrkCourseregistration\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkCourseregistration\Domain\Repository\CourseregistrationFormRepository;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Package\Exception;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileException;

/**
 * Class CourseregistrationFormLsController
 * @package DRK\DrkCourseregistration\Controller
 */
class CourseregistrationFormLsController extends CourseregistrationFormAbstractController
{

    /**
     * @var array
     */
    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name', 'strasse', 'plz', 'ort'],
        'company' => ['firma', 'strasse', 'plz', 'ort', 'vorname', 'name']
    ];

    /**
     * @var CourseregistrationFormRepository $courseregistrationFormRepository
     */
    protected CourseregistrationFormRepository $courseregistrationFormRepository;

    /**
     * set true, if waiting list is active and respectively overbooking should happen
     *
     * @var bool
     */
    protected bool $waitinglist_active = false;

    /**
     * @var array
     */
    public $error = [];

    /**
     * @var bool
     */
    private bool $isDog;
    /**
     * @var bool
     */
    private bool $isCompany;

    /**
     * @param CourseregistrationFormRepository $courseregistrationFormRepository
     */
    public function injectCourseRepository(CourseregistrationFormRepository $courseregistrationFormRepository): void
    {
        $this->courseregistrationFormRepository = $courseregistrationFormRepository;
    }

    /**
     * showSingleFormAction
     */
    public function showSingleFormAction()
    {
        return $this->showForm('');
    }

    /**
     * showCompanyFormAction
     */
    public function showCompanyFormAction()
    {
        return $this->showForm('full');
    }

    /**
     * showCompanyFormAction
     */
    public function showDogFormAction()
    {
        return $this->showForm('dog');
    }


    /**
     * @param string $formType
     * @return ResponseInterface
     * @throws \Exception
     */
    public function showForm(string $formType = ''): ResponseInterface
    {

        switch ($formType) {

            case 'dog':
                $this->isDog = true;
                $this->isCompany = false;
                break;
            case 'full':
                $this->isDog = false;
                $this->isCompany = true;
                break;
            default:
                $this->isDog = false;
                $this->isCompany = false;

        }

        // check for valid $courseEventId
        if (!is_numeric($this->courseEventId) || empty($this->courseEventId)) {
            $this->view->assign('Error', true);
            $this->error = ['Error' => 'Es wurde keine Kurs-Event-ID übergeben!'];
            $this->error_reporting();
        } else {
            $request = [
                $this->settings['apiKey'],
                $this->courseEventId
            ];

            /** Show extensionVersion */
            $this->extensionVersion = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion($this->request->getControllerExtensionKey());
            $this->view->assign('extensionVersion', $this->extensionVersion);

            // get course from webservice
            $courseData = $this->courseregistrationFormRepository->getLsCourseEvent($request);

            if (empty($courseData)) {
                $this->view->assign('Error', true);
                $this->error = ['Error' => 'Kurs konnte nicht gefunden werden oder ist bereits beendet!'];
                $this->error_reporting();
            } else {
                $this->view->assign('Error', false);

                /**
                 * select the right form for the company version
                 * - company professional association (0) (default)
                 * - single person as self payer (1)
                 */
                if ($this->isCompany) {
                    switch ($this->settings['defaultform']) {

                        case 1:
                            $this->view->assign('ShowDefault', array('company' => false, 'person' => true));
                            break;

                        default:
                            $this->view->assign('ShowDefault', array('company' => true, 'person' => false));
                    }
                }
                //put empty option to the top of the array for the select field
                if (!empty($courseData["payers"])) {
                    $courseData["payers"] = [""=>"Bitte auswählen"] + $courseData["payers"];
                }

                //set free_places && free_overnight_stay
                $this->view->assign(
                    'free_overnight_stay',
                    intval($courseData['course_event_freereservationrooms']) > 0 ? intval($courseData['course_event_freereservationrooms']) : 0
                );


                /**
                 * if overbooking, set subscribes if zero and waitinglist_active true
                 */
                if ((intval($courseData['course_event_freeplaces']) <= 0)) {
                    $_freeplaces = 2;
                    $this->waitinglist_active = true;
                } else {
                    $_freeplaces = intval($courseData['course_event_freeplaces']);
                    $this->waitinglist_active = false;
                }

                /**
                 * set number of subscriber
                 * minSubscriber allows overbooking
                 */
                if ($_freeplaces > $this->settings['minSubscribers']) {
                    $number_subscriber = min($_freeplaces, $this->settings['maxSubscribers']);
                } else {
                    $number_subscriber = $this->settings['minSubscribers'];
                }

                $this->view->assign('number_subscriber_array', range(1, $number_subscriber));
                $this->view->assign('number_subscriber', $number_subscriber);


                /**
                 * manipulate org name
                 */
                if (!empty($courseData['org_name'])) {

                    // clear DRK or BRK from Name
                    if (strpos(
                        strtolower($courseData['org_name']),
                        'drk'
                    ) !== false || strpos(strtolower($courseData['org_name']), 'brk') !== false) {
                        $courseData['org_name'] = str_ireplace('DRK', '', $courseData['org_name']);
                        $courseData['org_name'] = str_ireplace('BRK', '', $courseData['org_name']);
                    }

                    // now set the right prefix
                    if ((bool)$this->settings['isBRK']) {
                        $courseData['org_name'] = "BRK " . $courseData['org_name'];
                    } else {
                        $courseData['org_name'] = "DRK " . $courseData['org_name'];
                    }
                }

                $aShowFields = [];
                $aShowFields['pBirthday'] = (bool)$this->settings['showPrivatBirthdayField'];
                $aShowFields['Mail'] = (bool)$this->settings['showCompanyMailField'];
                $aShowFields['Mobile'] = (bool)$this->settings['showMobileField'];
                $aShowFields['Phone'] = (bool)$this->settings['showPhoneField'];
                $aShowFields['EmailDeclassification'] = (bool)$this->settings['showEmailDeclassificationField'];

                // array for form options
                $options = [];
                $options['prefix'] = \DRK\DrkGeneral\Utilities\Utility::$prefixArray;
                $options['title'] = \DRK\DrkGeneral\Utilities\Utility::$titleArray;
                $options['overnightstay'] = ['keine Übernachtung', 'Einzelzimmer', 'Doppelzimmer'];
                $options['meals'] = [
                    1 => 'Vollkost',
                    2 => 'vegetarische Verpflegung',
                    3 => 'vegane Verpflegung',
                    0 => 'keine Verpflegung'
                ];
                $options['honorary'] = [1 => 'ehrenamtlich tätig', 0 => 'hauptamtlich tätig'];
                $options['member'] = [1 => 'DRK Mitglied', 0 => 'kein DRK Mitglied'];
                $options['qualifcation'] = [
                    0 => 'keine Qualifikation',
                    1 => 'Qualifikation wird erfüllt',
                    2 => 'Qualifikation wird bis zum Lehrgangsbeginn erfüllt',
                    3 => 'Qualifikation ist unvollständig'
                ];

                $this->view->assign('course_event_id', $this->courseEventId);
                $this->view->assign('courseData', $courseData);
                $this->view->assign('options_array', $options);
                $this->view->assign('bPayingCash', (bool)$this->settings['payCash']);
                $this->view->assign('payCashText', $this->settings['payCashText']);
                $this->view->assign('aShowFields', $aShowFields);
                $this->view->assign(
                    'terms_url',
                    $this->validateUrl($this->settings['termsUrl']) ? $this->settings['termsUrl'] : ''
                );
                $this->view->assign(
                    'privacy_url',
                    $this->validateUrl($this->settings['privacyUrl']) ? $this->settings['privacyUrl'] : ''
                );
                $this->view->assign('sOverbookingForbiddenMessage', $this->settings['overbookingForbiddenMessage']);
                $this->view->assign('bWaitinglist_active', $this->waitinglist_active);
                $this->view->assign('sWaitinglist_message', $this->settings['waitinglistMessage']);
            }
        }

        return $this->htmlResponse();
    }

    /**
     * sendAction
     *
     * @return ResponseInterface|void
     * @throws \Exception
     */
    public function sendAction()
    {
        $aSendObject = [];
        $course_event_id = 0;
        $isCompany = 0;
        $isDog = 0;
        $this->view->assign('sending_ok', false);
        $aArguments = $this->request->getArguments();

        $errors = false;

        $aFormData = [];

        if (!empty($aArguments)) {
            array_key_exists(
                'anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['titel'] : $aFormData['titel'] = 1;
            array_key_exists('name', $aArguments) ? $aFormData['name'] = $aArguments['name'] : $aFormData['name'] = "";
            array_key_exists(
                'vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'strasse',
                $aArguments
            ) ? $aFormData['strasse'] = $aArguments['strasse'] : $aFormData['strasse'] = "";
            array_key_exists('ort', $aArguments) ? $aFormData['ort'] = $aArguments['ort'] : $aFormData['ort'] = "";
            array_key_exists('plz', $aArguments) ? $aFormData['plz'] = $aArguments['plz'] : $aFormData['plz'] = "";
            array_key_exists(
                'geburtsdatum',
                $aArguments
            ) ? $aFormData['geburtsdatum'] = $aArguments['geburtsdatum'] : $aFormData['geburtsdatum'] = null;
            array_key_exists(
                'email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['email'] : $aFormData['email'] = "";
            array_key_exists(
                'telefon',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['telefon'] : $aFormData['telefon'] = "";
            array_key_exists(
                'mobile',
                $aArguments
            ) ? $aFormData['mobile'] = $aArguments['mobile'] : $aFormData['mobile'] = "";
            array_key_exists(
                'bank_accountowner',
                $aArguments
            ) ? $aFormData['bank_accountowner'] = $aArguments['bank_accountowner'] : $aFormData['bank_accountowner'] = "";
            array_key_exists(
                'kontotyp',
                $aArguments
            ) ? $aFormData['kontotyp'] = $aArguments['kontotyp'] : $aFormData['kontotyp'] = "SEPA";
            array_key_exists(
                'kontonummer',
                $aArguments
            ) ? $aFormData['kontonummer'] = $aArguments['kontonummer'] : $aFormData['kontonummer'] = "";
            array_key_exists('blz', $aArguments) ? $aFormData['blz'] = $aArguments['blz'] : $aFormData['blz'] = "";
            array_key_exists('iban', $aArguments) ? $aFormData['iban'] = $aArguments['iban'] : $aFormData['iban'] = "";
            array_key_exists('bank', $aArguments) ? $aFormData['bank'] = $aArguments['bank'] : $aFormData['bank'] = "";
            array_key_exists(
                'email_freigabe',
                $aArguments
            ) ? $aFormData['email_freigabe'] = $aArguments['email_freigabe'] : $aFormData['email_freigabe'] = 0;
            array_key_exists(
                'course_event_id',
                $aArguments
            ) ? $course_event_id = intval($aArguments['course_event_id']) : 0;

            array_key_exists(
                'honorary',
                $aArguments
            ) ? $aFormData['honorary'] = $aArguments['honorary'] : $aFormData['honorary'] = 0;
            array_key_exists(
                'member',
                $aArguments
            ) ? $aFormData['member'] = $aArguments['member'] : $aFormData['member'] = 0;
            array_key_exists(
                'overnightstay',
                $aArguments
            ) ? $aFormData['overnightstay'] = $aArguments['overnightstay'] : $aFormData['overnightstay'] = 0;
            array_key_exists(
                'meals',
                $aArguments
            ) ? $aFormData['meals'] = $aArguments['meals'] : $aFormData['meals'] = 0;
            array_key_exists(
                'qualifcation',
                $aArguments
            ) ? $aFormData['qualifcation'] = $aArguments['qualifcation'] : $aFormData['qualifcation'] = 0;
            array_key_exists(
                'note',
                $aArguments
            ) ? $aFormData['note'] = $aArguments['note'] : $aFormData['note'] = null;
            array_key_exists(
                'ordercode',
                $aArguments
            ) ? $aFormData['ordercode'] = $aArguments['ordercode'] : $aFormData['ordercode'] = null;
            array_key_exists(
                'birthplace',
                $aArguments
            ) ? $aFormData['birthplace'] = $aArguments['birthplace'] : $aFormData['birthplace'] = null;
            array_key_exists(
                'function',
                $aArguments
            ) ? $aFormData['function'] = $aArguments['function'] : $aFormData['function'] = null;

            // Organisation of person
            array_key_exists(
                'org_name',
                $aArguments
            ) ? $aFormData['org_name'] = $aArguments['org_name'] : $aFormData['org_name'] = null;
            array_key_exists(
                'org_street',
                $aArguments
            ) ? $aFormData['org_street'] = $aArguments['org_street'] : $aFormData['org_street'] = null;
            array_key_exists(
                'org_zip',
                $aArguments
            ) ? $aFormData['org_zip'] = $aArguments['org_zip'] : $aFormData['org_zip'] = null;
            array_key_exists(
                'org_city',
                $aArguments
            ) ? $aFormData['org_city'] = $aArguments['org_city'] : $aFormData['org_city'] = null;

            //if course_waitinglist isset, then set as FormData
            array_key_exists(
                'course_waitinglist',
                $aArguments
            ) ? $aFormData['course_waitinglist'] = intval($aArguments['course_waitinglist']) : 0;

            // set number of reservations: 1 Person
            $this->resvertions = 1;
        }

        //if is dog registration
        if (array_key_exists('isdog', $aArguments) && intval($aArguments['isdog']) > 0) {
            $isDog = 1;

            array_key_exists(
                'dog_name',
                $aArguments
            ) ? $aFormData['dog_name'] = $aArguments['dog_name'] : $aFormData['dog_name'] = null;

            array_key_exists(
                'dog_birthday',
                $aArguments
            ) ? $aFormData['dog_birthday'] = $aArguments['dog_birthday'] : null;

            array_key_exists(
                'dog_sex',
                $aArguments
            ) ? $aFormData['dog_sex'] = $aArguments['dog_sex'] : 0;

            array_key_exists(
                'dog_badge',
                $aArguments
            ) ? $aFormData['dog_badge'] = $aArguments['dog_badge'] : null;
        }

        //if is company registration
        if (array_key_exists('iscompany', $aArguments) && intval($aArguments['iscompany']) > 0) {
            $isCompany = 1;

            array_key_exists(
                'firma',
                $aArguments
            ) ? $aFormData['firma'] = $aArguments['firma'] : $aFormData['firma'] = "";
            array_key_exists(
                'company_phone',
                $aArguments
            ) ? $aFormData['company_phone'] = $aArguments['company_phone'] : $aFormData['company_phone'] = "";
            array_key_exists(
                'company_email',
                $aArguments
            ) ? $aFormData['company_email'] = $aArguments['company_email'] : $aFormData['company_email'] = "";
            array_key_exists(
                'company_kontotyp',
                $aArguments
            ) ? $aFormData['kontotyp'] = $aArguments['company_kontotyp'] : $aFormData['kontotyp'] = "SEPA";

            array_key_exists(
                'contact_anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['contact_anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'contact_titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['contact_titel'] : $aFormData['titel'] = 1;
            array_key_exists(
                'contact_name',
                $aArguments
            ) ? $aFormData['name'] = $aArguments['contact_name'] : $aFormData['name'] = "";
            array_key_exists(
                'contact_vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['contact_vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'contact_phone',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['contact_phone'] : $aFormData['telefon'] = "";
            array_key_exists(
                'contact_mobile',
                $aArguments
            ) ? $aFormData['mobile'] = $aArguments['contact_mobile'] : $aFormData['mobile'] = "";
            array_key_exists(
                'contact_email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['contact_email'] : $aFormData['email'] = "";

            //get subscriber
            if (array_key_exists('sub', $aArguments) && is_array($aArguments['sub'])) {
                $i = 1;
                foreach ($aArguments['sub'] as $subscriber) {
                    // min set is name
                    if (!empty($subscriber['name'])) {
                        !empty($subscriber['anrede']) ? $aFormData['subscriber'][$i]['anrede'] = $subscriber['anrede'] : $aFormData['subscriber'][$i]['anrede'] = 1;
                        !empty($subscriber['vorname']) ? $aFormData['subscriber'][$i]['vorname'] = $subscriber['vorname'] : $aFormData['subscriber'][$i]['vorname'] = "";

                        if (!empty($subscriber['geburtsdatum'])) {
                            $aFormData['subscriber'][$i]['geburtsdatum'] = date(
                                'Y-m-d',
                                strtotime($subscriber['geburtsdatum'])
                            );
                        } elseif ((bool)$this->settings['showCompanyBirthdayField'] && (bool)$this->settings['requireCompanyBirthdayField']) {
                            $this->error = ['Error' => 'Das Geburtsdatum eines der Teilnehmer fehlt.'];
                            $errors = true;
                        }

                        $aFormData['subscriber'][$i]['name'] = $subscriber['name'];

                        !empty($subscriber['phone']) ? $aFormData['subscriber'][$i]['phone'] = $subscriber['phone'] : $aFormData['subscriber'][$i]['phone'] = null;
                        !empty($subscriber['mobile']) ? $aFormData['subscriber'][$i]['mobile'] = $subscriber['mobile'] : $aFormData['subscriber'][$i]['mobile'] = null;
                        !empty($subscriber['email']) ? $aFormData['subscriber'][$i]['email'] = $subscriber['email'] : $aFormData['subscriber'][$i]['email'] = null;
                        !empty($subscriber['strasse']) ? $aFormData['subscriber'][$i]['strasse'] = $subscriber['strasse'] : $aFormData['subscriber'][$i]['strasse'] = null;
                        !empty($subscriber['ort']) ? $aFormData['subscriber'][$i]['ort'] = $subscriber['ort'] : $aFormData['subscriber'][$i]['ort'] = null;
                        !empty($subscriber['plz']) ? $aFormData['subscriber'][$i]['plz'] = $subscriber['plz'] : $aFormData['subscriber'][$i]['plz'] = null;
                        !empty($subscriber['overnightstay']) ? $aFormData['subscriber'][$i]['overnightstay'] = $subscriber['overnightstay'] : $aFormData['subscriber'][$i]['overnightstay'] = 0;
                        !empty($subscriber['qualifcation']) ? $aFormData['subscriber'][$i]['qualifcation'] = $subscriber['qualifcation'] : $aFormData['subscriber'][$i]['qualifcation'] = 0;
                        !empty($subscriber['meals']) ? $aFormData['subscriber'][$i]['meals'] = $subscriber['meals'] : $aFormData['subscriber'][$i]['meals'] = 0;
                        !empty($subscriber['honorary']) ? $aFormData['subscriber'][$i]['honorary'] = $subscriber['honorary'] : $aFormData['subscriber'][$i]['honorary'] = 0;
                        !empty($subscriber['member']) ? $aFormData['subscriber'][$i]['member'] = $subscriber['member'] : $aFormData['subscriber'][$i]['member'] = 0;
                        !empty($subscriber['note']) ? $aFormData['subscriber'][$i]['note'] = $subscriber['note'] : $aFormData['subscriber'][$i]['note'] = null;
                        !empty($subscriber['ordercode']) ? $aFormData['subscriber'][$i]['ordercode'] = $subscriber['ordercode'] : $aFormData['subscriber'][$i]['ordercode'] = null;
                        !empty($subscriber['function']) ? $aFormData['subscriber'][$i]['function'] = $subscriber['function'] : $aFormData['subscriber'][$i]['function'] = null;
                        !empty($subscriber['birthplace']) ? $aFormData['subscriber'][$i]['birthplace'] = $subscriber['birthplace'] : $aFormData['subscriber'][$i]['birthplace'] = null;

                        $i++;
                    }
                }
            }
            //min one subscriber is needed
            if (!isset($aFormData['subscriber']) || count($aFormData['subscriber']) == 0) {
                $this->error = ['Error' => 'Mindesten ein Teilnehmer muss eingetragen werden.'];
                $errors = true;
            } else {
                // set number of resvertions: count of subscriber
                $this->resvertions = count($aFormData['subscriber']);
            }

            // check for bg form
            if (intval($this->settings['showBGField']) == 1) {
                $aFormData['company_bg_number'] = array_key_exists(
                    'company_bg_number',
                    $aArguments
                ) ? $aArguments['company_bg_number'] : null;
                $aFormData['company_payers'] = array_key_exists(
                    'company_payers',
                    $aArguments
                ) ? $aArguments['company_payers'] : 0;
            }
        }

        //check term-check
        if (
            $this->validateUrl($this->settings['termsUrl'] ??= '') &&
            (!array_key_exists('terms_check', $aArguments) || intval($aArguments['terms_check']) != 1)
        ) {
            $this->error = ['Error' => 'Sie haben den AGB nicht zugestimmt.'];
            $errors = true;
        }

        // check course_event_id
        if (empty($course_event_id)) {
            $this->error = ['Error' => 'Es wurde keine Kursnummer übergeben!'];
            $errors = true;
        }

        $aSendObject[0] = $this->settings['apiKey'];
        $aSendObject[1] = $aFormData;
        $aSendObject[2] = $course_event_id;

        // set form type company or person or dog
        if ($isCompany) {
            $aSendObject[3] = 'company';
        }
        elseif ($isDog) {
            $aSendObject[3] = 'dog';
        }
        else {
            $aSendObject[3] = 'person';
        }

        //check if we have an empty form
        $sCheck = $aArguments['name'] ??= '' . $aArguments['strasse'] ??= '' . $aArguments['firma'] ??= '' . $aArguments['bank_accountowner'] ??= '';
        if (empty($sCheck)) {
            $this->error = ['Error' => 'Bitte kein leeres Formular absenden!'];
            $errors = true;
        }

        // Add required fields from required settings
        // $plugin.tx_drkcourseregistration.settings.Required
        foreach ($this->settings['Required'] as $fieldname => $required) {
            if ($required) {
                if (strpos($fieldname, 'company') !== false) {
                    array_push($this->mandatoryFormFields['company'], $fieldname);
                } elseif (strpos($fieldname, 'company') === false) {
                    array_push($this->mandatoryFormFields['person'], $fieldname);
                    array_push($this->mandatoryFormFields['company'], $fieldname);
                }
            }
        }

        if (!$isCompany) {
            // Special handling for requirePrivatBirthdayField
            if ($this->settings['showPrivatBirthdayField'] && $this->settings['requirePrivatBirthdayField']) {
                array_push($this->mandatoryFormFields['person'], 'geburtsdatum');
            }

            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);
        } else {
            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['company'], $aFormData);
        }

        //check if form send twice
        $aFormSessionInfo = $GLOBALS['TSFE']->fe_user->getKey('ses', 'CourseregistrationLsForm');
        $iWaitTimeResendForm = is_numeric($this->settings['WaitTimeResendForm']) && $this->settings['WaitTimeResendForm'] > 0 ? $this->settings['WaitTimeResendForm'] : 600;

        if ((is_array($aFormSessionInfo) &&
            $aFormSessionInfo['time'] + $iWaitTimeResendForm >= time() &&
            $aFormSessionInfo['hash'] == md5(serialize($aSendObject))) &&
            !$this->debug)
        {
            $this->error = ['Error' => 'Das Formular bitte nicht mehrfach absenden!'];
            $errors = true;
        }

        // get fresh CourseData for last checks and ThanksPage
        $aParams[0] = $this->settings['apiKey'];
        $aParams[1] = $course_event_id;

        if (!$aCourseData = $this->courseregistrationFormRepository->getLsCourseEvent($aParams)) {
            $this->error = ['Error' => 'Der Gebuchte Kurs wurde nicht mehr gefunden!'];
            $errors = true;
        }

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['birthname'] ?? '') || !$this->isHoneypotFilled($aArguments['contact_birthname'] ?? ''))
        {
            $errors = true;
        }
        if (!$errors && $mandatoryFieldsFilled) {

            // sending Data are successful
            if ($this->courseregistrationFormRepository->sendCustomerData($aSendObject, 'LS')) {
                $this->view->assign('sending_ok', true);

                //check for possible overbooking
                if (intval($aCourseData['course_event_freeplaces']) < $this->resvertions) {
                    $this->view->assign('bWaitinglist_active', true);
                } else {
                    $this->view->assign('bWaitinglist_active', false);
                }

                // show Warnings
                if (!empty($sWarnings = $this->courseregistrationFormRepository->getErrors())) {
                    $this->view->assign('warningMessage', $sWarnings);
                }

                //set form fingerprint to session to block sending twice
                $aFormSessionInfo['hash'] = md5(serialize($aSendObject));
                $aFormSessionInfo['time'] = time();
                $GLOBALS['TSFE']->fe_user->setKey('ses', 'CourseregistrationLsForm', $aFormSessionInfo);

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->debug) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uri = $uriBuilder->build();
                    $this->redirectToUri($uri);
                    exit;
                } // else go on and render the template
                else {
                    unset($aSendObject);

                    $aClientData = $aFormData;
                    $aClientData['anrede'] = array_key_exists(
                        $aFormData['anrede'],
                        \DRK\DrkGeneral\Utilities\Utility::$prefixArray
                    ) ? \DRK\DrkGeneral\Utilities\Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                    $aClientData['titel'] = array_key_exists(
                        $aFormData['titel'],
                        \DRK\DrkGeneral\Utilities\Utility::$titleArray
                    ) ? \DRK\DrkGeneral\Utilities\Utility::$titleArray[$aFormData['titel']] : "";

                    $this->view->assign('aCourseData', $aCourseData);
                    $this->view->assign('aClientData', $aClientData);
                    $this->view->assign('isCompany', $isCompany);
                    $this->view->assign('isDog', $isDog);
                    $this->view->assign('sWaitinglist_message', $this->settings['waitinglistMessage']);
                }
            }
        }

        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();

        } else {
            $this->view->assign('error', false);
        }

        return $this->htmlResponse();
    }

    /**
     * Init
     *
     * @return void
     * @throws InvalidFileException
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addJavaScript(
            'courseregistration-ls-js',
            'EXT:drk_courseregistration/Resources/Public/Scripts/tx_drkcourseregistrationform_ls.js',
            [],
            ['priority' => false] // lädt in footer
        );
    }
}
