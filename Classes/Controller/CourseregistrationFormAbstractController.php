<?php

namespace DRK\DrkCourseregistration\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Http\RequestFactory;
use \TYPO3\CMS\Core\Log\LogManager;
use \TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Utility\PathUtility;
use Psr\Http\Message\ResponseInterface;


/**
 * Class CourseregistrationFormAbstractController
 * @package DRK\DrkCourseregistration\Controller
 */
abstract class CourseregistrationFormAbstractController extends AbstractDrkController
{

    /**
     * Number of resversions of places
     *
     * @var int $resvertions
     */
    protected int $resvertions = 0;

    /**
     * Id of CourseEvent
     *
     * @var int
     */
    protected int $courseEventId = 0;

    /**
     * Version of Extension
     *
     * @var string
     */
    protected string $extensionVersion = '';

    /**
     * @var object
     */
    protected object $pageRenderer;

    /**
     * @var string
     */
    protected string $siteRelPath = '';

    /** @var RequestFactory*/
    protected RequestFactory $requestFactory;

    /** @var Logger */
    protected Logger $logger;

    /** @var string $paypal_api_url */
    protected string $paypal_api_url = "";

    /** @var bool $paypal_is_active */
    protected bool $paypal_is_active = false;

    /** @var bool  $debug */
    protected bool $debug = false;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        protected readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * @return void
     * @throws \TYPO3\CMS\Core\Resource\Exception\InvalidFileException
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        /** @var RequestFactory $requestFactory */
        $this->requestFactory = GeneralUtility::makeInstance(RequestFactory::class);

        /** @var $logger \TYPO3\CMS\Core\Log\Logger */
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);

        /** @var \TYPO3\CMS\Core\Http\ServerRequest $typo3Request */
        $typo3Request = $GLOBALS['TYPO3_REQUEST'] ?? null;

        if ($typo3Request instanceof ServerRequestInterface) {
            $queryArguments = $typo3Request->getAttribute('routing');
            if ($queryArguments instanceof PageArguments) {
                $arguments = $queryArguments->getArguments();
                $this->courseEventId = (int)($arguments['courseeventid'] ?? 0);
            }
        }

        $this->assetCollector->addStyleSheet(
            'courseregistration-styles',
            'EXT:drk_courseregistration/Resources/Public/Css/styles.css'
        );
        $this->assetCollector->addStyleSheet(
            'courseregistration-modal',
            'EXT:drk_courseregistration/Resources/Public/Css/jquery.modal.min.css'
        );
        $this->assetCollector->addJavaScript(
            'courseregistration-js',
            'EXT:drk_courseregistration/Resources/Public/Scripts/tx_drkcourseregistrationform.js',
            [],
            ['priority' => false] // lädt in footer
        );
        /** load jquery.maskedinput */
        $this->assetCollector->addJavaScript(
            'courseregistration-js-maskinput',
            'EXT:drk_courseregistration/Resources/Public/Scripts/jquery.maskedinput.min.js',
            [],
            ['priority' => false] // lädt in footer
        );
        /** load jquery.modal  */
        $this->assetCollector->addJavaScript(
            'courseregistration-js-modal',
            'EXT:drk_courseregistration/Resources/Public/Scripts/jquery.modal.min.js',
            [],
            ['priority' => false] // lädt in footer
        );
        /**
         * PayPal init
         */

        $this->paypal_is_active = boolval($this->settings['Paypal']['Active']);

        // check for existing api base url
        if (empty($this->settings['Paypal']['ApiBaseUrl'])) {
            $this->logger->critical('PayPal is deactivated: Empty PayPal api base url!');
            $this->paypal_is_active = false;
        }

        // check for existing credentials
        if (empty($this->settings['Paypal']['ClientID']) || empty($this->settings['Paypal']['ClientSecret'])) {
            $this->logger->critical('PayPal is deactivated: PayPal Client ID or Client Secret no set!');
            $this->paypal_is_active = false;
        }

        if ($this->paypal_is_active) {

            // set PayPal API Base URL
            $this->paypal_api_url = $this->settings['Paypal']['ApiBaseUrl'];

            // disable fundings
            $funding_to_disable =  [];
            if (1 != intval($this->settings['Paypal']['Sepa'])) { $funding_to_disable[]= 'sepa'; }
            if (1 != intval($this->settings['Paypal']['Card'])) { $funding_to_disable[]= 'card'; }

            if (empty($funding_to_disable)) {
                $disable_funding = '';

            } else {
                $disable_funding = '&disable-funding=' . implode( ',', $funding_to_disable);
            }
            $this->assetCollector->addInlineJavaScript(
                'courseregistrationp-paypal-settings',
                "const paypal_src = 'https://www.paypal.com/sdk/js?client-id=". $this->settings['Paypal']['ClientID'] .
                "&currency=EUR&locale=de_DE&intent=authorize$disable_funding';",
                [],
                ['priority' => true] // lädt in header
            );

            /**
             * If GDPR option is used, deactivate PayPal autoload
             */
            if ($this->settings['Paypal']['GDPR']) {
                $this->assetCollector->addInlineJavaScript(
                    "courseregistrationp-paypal-gdpr",
                    "
                    $('#paypal_gdpr').click(function(){
                        $.getScript('" .
                    PathUtility::getPublicResourceWebPath ('EXT:drk_courseregistration/Resources/Public/Scripts/tx_drkcourseregistrationform_paypal.js') .
                    "');
                        $('#paypal_consent').hide();
                    });"
                );
            } else {
                $this->assetCollector->addJavaScript(
                   'courseregistrationp-paypal-lib',
                   'EXT:drk_courseregistration/Resources/Public/Scripts/tx_drkcourseregistrationform_paypal.js',
                    [],
                    ['priority' => false] // lädt in footer
               );
            }
        }

        /** @var debug */
        $this->debug = $this->settings['Debug'] ?? false ;
    }


    /**
     *
     * error_reporting
     *
     */
    protected function error_reporting(): void
    {
        $errors = array_merge($this->courseregistrationFormRepository->getErrors(), $this->error);
        $this->view->assign('errorMessages', $errors);
    }

    /**
     * Capture a PayPal Payment
     *
     * @param string $paypal_order_id
     * @param string $paypal_auth_id
     * @return bool
     */
    protected function CapturePaypalPayment(string $paypal_order_id, string $paypal_auth_id): bool
    {
        $paypal_access_token = "";

        if (empty($paypal_order_id) || empty($paypal_auth_id)) {
            return false;
        }

        /**
         * get PayPal Bearer access token
         */
        $additionalOptions = [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'auth' => [$this->settings['Paypal']['ClientID'], $this->settings['Paypal']['ClientSecret']],
            'body' => 'grant_type=client_credentials',
        ];

        try {
            $response = $this->requestFactory->request(
                $this->paypal_api_url . "/v1/oauth2/token",
                'POST',
                $additionalOptions
            );
        }
        catch (ClientException $e) {
            return false;
        }

        $res_body = json_decode($response->getBody());

        if ($response->getStatusCode() != 200 || empty($res_body->access_token)) {
            return false;
        }
        $paypal_access_token = $res_body->access_token;


        /**
         * Capture PayPal Payment
         */
        $additionalOptions = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$paypal_access_token,

            ],
        ];
        try {
            $response = $this->requestFactory->request(
                $this->paypal_api_url."/v2/payments/authorizations/{$paypal_auth_id}/capture",
                'POST',
                $additionalOptions
            );
        }
        catch (ClientException $e) {
            return false;
        }
        $res_body = json_decode($response->getBody());

        if ($response->getStatusCode() != 201 || $res_body->status != 'COMPLETED') {
            return false;
        }

        return true;
    }

    /**
     * Cancel PayPal Order
     *
     * @param string $paypal_auth_id
     * @return bool
     */
    protected function CancelOrder(string $paypal_auth_id): bool {

        if (empty($paypal_auth_id)) {
            return false;
        }

        /**
         * get PayPal Bearer access token
         */
        $additionalOptions = [
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            'auth' => [$this->settings['Paypal']['ClientID'], $this->settings['Paypal']['ClientSecret']],
            'body' => 'grant_type=client_credentials',
        ];

        $response = $this->requestFactory->request(
            $this->paypal_api_url."/v1/oauth2/token",
            'POST',
            $additionalOptions
        );

        $res_body = json_decode($response->getBody());

        if ($response->getStatusCode() != 200 || empty($res_body->access_token)) {
            return false;
        }
        $paypal_access_token = $res_body->access_token;

        // void payment

        $additionalOptions = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$paypal_access_token,

            ],
        ];

        $response = $this->requestFactory->request(
            $this->paypal_api_url."/v2/payments/authorizations/{$paypal_auth_id}/void",
            'POST',
            $additionalOptions
        );

        if ($response->getStatusCode() != 204) {
            return false;
        }

        return true;

    }
}
