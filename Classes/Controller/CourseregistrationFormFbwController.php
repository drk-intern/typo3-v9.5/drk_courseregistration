<?php

namespace DRK\DrkCourseregistration\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkCourseregistration\Domain\Repository\CourseregistrationFormRepository;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Package\Exception;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileException;

/**
 * Class CourseregistrationFormFbwController
 * @package DRK\DrkCourseregistration\Controller
 */
class CourseregistrationFormFbwController extends CourseregistrationFormAbstractController
{

    /**
     * @var array
     */
    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name', 'strasse', 'plz', 'ort'],
        'company' => ['firma', 'strasse', 'plz', 'ort', 'vorname', 'name']
    ];

    /**
     * @var bool
     */
    protected bool $isChild = false;

    /**
     * @var bool
     */
    protected bool $isCompany = false;

    /**
     * @var CourseregistrationFormRepository $courseregistrationFormRepository
     */
    protected CourseregistrationFormRepository $courseregistrationFormRepository;

    /**
     * fits to $prefix_array with 1 => Herr and 2 => Frau
     *
     * @var array
     */
    protected array $prefix_child_array = [29 => '', 1 => 'Junge', 2 => 'Mädchen'];

    /**
     * set true, if waiting list is active and respectively overbooking should happen
     *
     * @var bool
     */
    protected bool $waitinglist_active = false;

    /**
     * @param CourseregistrationFormRepository $courseregistrationFormRepository
     */
    public function injectCourseRepository(CourseregistrationFormRepository $courseregistrationFormRepository)
    {
        $this->courseregistrationFormRepository = $courseregistrationFormRepository;
    }

    /**
     * showChildrenFormAction
     * @throws Exception
     */
    public function showChildrenFormAction()
    {
        return $this->showForm('children');
    }

    /**
     * showSingleFormAction
     * @throws Exception
     */
    public function showSingleFormAction()
    {
        return $this->showForm('single');
    }

    /**
     * showCompanyFormAction
     * @throws Exception
     */
    public function showCompanyFormAction()
    {
        return $this->showForm('full');
    }


    /**
     * @param string $formType - Types are: children, later more
     * @return ResponseInterface
     * @throws Exception
     */
    public function showForm(string $formType = 'children'): ResponseInterface
    {
        switch ($formType) {

            case 'children':
                $this->isChild = true;
                $this->isCompany = false;
                break;
            case 'full':
                $this->isChild = false;
                $this->isCompany = true;
                break;
            default:
                $this->isChild = false;
                $this->isCompany = false;

        }

        /** Show extensionVersion */
        $this->extensionVersion = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion($this->request->getControllerExtensionKey());
        $this->view->assign('extensionVersion', $this->extensionVersion);

        // check for valid $courseEventId
        if (!is_numeric($this->courseEventId) || empty($this->courseEventId)) {
            $this->view->assign('Error', true);
            $this->error = ['Error' => 'Es wurde keine Kurs-Event-ID übergeben!'];
            $this->error_reporting();
        } else {
            $request = [
                $this->settings['apiKey'],
                $this->courseEventId
            ];


            // get course from webservice
            $courseData = $this->courseregistrationFormRepository->getFbwCourseEvent($request);

            if (empty($courseData)) {
                $this->view->assign('Error', true);
                $this->error = ['Error' => 'Kurs konnte nicht gefunden werden oder ist bereits beendet!'];
                $this->error_reporting();
            } else {
                $this->view->assign('Error', false);


                /**
                 * set the reductions for that course
                 *
                 * this array come from the service
                 * reductions => array
                 *       4 => array
                 *           name => '3 Kinder'
                 *           reduction_value => '2.00'
                 *           reductions_value_type => 'amount'
                 *       1 => array
                 *           name => 'Alleinerziehende'
                 *           reduction_value => '10.15'
                 *           reductions_value_type => 'amount'
                 *       3 => array
                 *           name => 'Arbeitslos'
                 *           reduction_value => '25.00'
                 *           reductions_value_type => 'percent'
                 * the reduction_value can be amount or percent - set in reductions_value_type
                 * put empty option to the top of the array for the select field
                 */
                if (!empty($courseData["reductions"])) {
                    $aReductions =
                        [0 =>
                            ["name" => "ohne Ermäßigung", "reduction_value" => "0",
                            "reductions_value_type" => 'amount']] +
                        $courseData["reductions"];
                    unset($courseData["reductions"]);

                    foreach ($aReductions as $iReductionId => $aReduction) {
                            $oReduction = new \stdClass;
                            $oReduction->id = $iReductionId;
                            $oReduction->name = $aReduction['name'];
                            $oReduction->value = doubleval($aReduction['reduction_value']);
                            $oReduction->value_type = $aReduction['reductions_value_type'];
                            $courseData["reductions"][] = $oReduction;
                    }
                }
                /**
                 * if overbooking, set subscribes if zero
                 * only adults are subscriber, children are not counted
                 */
                if (intval($courseData['course_event_freeplaces']) <= 0) {
                    $_freeplaces = 1;
                    $this->waitinglist_active = true;
                } else {
                    $_freeplaces = intval($courseData['course_event_freeplaces']);
                    $this->waitinglist_active = false;
                }

                /**
                 * set number of children
                 */
                if ($this->isChild) {
                    $this->view->assign(
                        'number_children',
                        range(0, intval($this->settings['maxCountOfChildren'] - 1))
                    );
                } elseif ($this->isCompany) {
                    $this->view->assign(
                        'number_subscriber',
                        $_freeplaces > 0 ?
                            range(0, (min($_freeplaces, $this->settings['maxSubscribers'])) - 1) : []
                    );
                } else {
                    $this->view->assign(
                        'number_subscriber', $_freeplaces
                    );
                }


                /**
                 * manipulate org name
                 */
                if (!empty($courseData['org_name'])) {

                    // clear DRK or BRK from Name
                    if (strpos(strtolower($courseData['org_name']), 'drk') !== false
                        || strpos(strtolower($courseData['org_name']), 'brk') !== false) {
                        $courseData['org_name'] = str_ireplace('DRK', '', $courseData['org_name']);
                        $courseData['org_name'] = str_ireplace('BRK', '', $courseData['org_name']);
                    }

                    // now set the right prefix
                    if ((bool)$this->settings['isBRK']) {
                        $courseData['org_name'] = "BRK " . $courseData['org_name'];
                    } else {
                        $courseData['org_name'] = "DRK " . $courseData['org_name'];
                    }
                }

                if ($this->isCompany) {
                    // Flexform value: defaultform 2 == person, 1    == company
                    if (intval($this->settings['defaultform']) == 1) {
                        $this->view->assign(
                            'ShowDefault',
                            array('company' => true, 'person' => false)
                        );
                    } else {
                        $this->view->assign(
                            'ShowDefault',
                            array('company' => false, 'person' => true)
                        );
                    }
                }

                /**
                 * deactivate payments for free courses
                 */
                if (empty($courseData['course_event_price']) || intval($courseData['course_event_price']) == 0)
                {
                    $_payCash = true;
                    $_payPal = false;
                    $_showReductionField = false;
                    $courseData['course_event_price'] = 0;
                    $this->settings['payCashText'] = "";
                    $_PaymentInCashHeader = '';
                } else
                {
                    $_payCash = (bool)$this->settings['payCash'];
                    $_payPal = $this->paypal_is_active;
                    $_showReductionField = (bool)$this->settings['showReductionField'] ?? false;
                    $_PaymentInCashHeader = $this->settings['PaymentInCashHeader'];
                }

                /**
                 * assign var
                 */

                $aShowFields = [];
                $aShowFields['pBirthday'] = (bool)$this->settings['showPrivatBirthdayField'];
                $aShowFields['Mail'] = (bool)$this->settings['showCompanyMailField'];
                $aShowFields['Mobile'] = (bool)$this->settings['showMobileField'];
                $aShowFields['Phone'] = (bool)$this->settings['showPhoneField'];
                $aShowFields['EmailDeclassification'] = (bool)$this->settings['showEmailDeclassificationField'];
                $aShowFields['Reductions'] = $_showReductionField;

                $aShowFields['cBirthday'] = (bool)$this->settings['showCompanyBirthdayField'];
                $aShowFields['SubscriberAddress'] = (bool)$this->settings['showSubscriberAddress'];

                $this->view->assign('course_event_id', $this->courseEventId);
                $this->view->assign('courseData', $courseData);
                $this->view->assign('course_form_typ', $formType);
                $this->view->assign('prefix_array', \DRK\DrkGeneral\Utilities\Utility::$prefixArray);
                $this->view->assign('prefix_child_array', $this->prefix_child_array);
                $this->view->assign('title_array', \DRK\DrkGeneral\Utilities\Utility::$titleArray);
                $this->view->assign('bPayingCash', $_payCash);
                $this->view->assign('bWaitinglist_active', $this->waitinglist_active);
                $this->view->assign('sWaitinglist_message', $this->settings['waitinglistMessage']);
                $this->view->assign('payCashText', $this->settings['payCashText']);
                $this->view->assign('aShowFields', $aShowFields);
                $this->view->assign('isCompany', $this->isCompany);
                $this->view->assign(
                    'terms_url',
                    $this->validateUrl($this->settings['termsUrl']) ? $this->settings['termsUrl'] : ''
                );
                $this->view->assign(
                    'privacy_url',
                    $this->validateUrl($this->settings['privacyUrl']) ? $this->settings['privacyUrl'] : ''
                );

                $this->view->assign('bPayPal',
                    !$this->isCompany && ((!$this->waitinglist_active ? $_payPal : false))); //TODO: Check Paypal for Companies
                $this->view->assign('sOverbookingForbiddenMessage', $this->settings['overbookingForbiddenMessage']);
                $this->view->assign('sPaymentInCashHeader', $_PaymentInCashHeader);
            }
        }

        return $this->htmlResponse();
    }

    /**
     * sendAction
     *
     * prepare form data befor send
     *
     * @return ResponseInterface|void
     */
    public function sendAction()
    {
        $aSendObject = [];
        $this->isChild = false;
        $course_event_id = 0;
        $this->isCompany = false;
        $this->view->assign('sending_ok', false);
        $aArguments = $this->request->getArguments();
        $paypal_authorized = false;
        $paypal_payment_captured = false;
        $paypal_order_id = "";
        $paypal_auth_id = "";

        $errors = false;

        $aFormData = [];

        switch ($aArguments['course_form_typ']) {

            case 'children':
                $this->isChild = true;
                $this->isCompany = false;
                break;
            case 'full':
                $this->isChild = false;
                $this->isCompany = isset($aArguments['iscompany']) ? boolval($aArguments['iscompany']) : false;
                break;
            default:
                $this->isChild = false;
                $this->isCompany = false;

        }

        $this->view->assign('isChild', $this->isChild);
        $this->view->assign('isCompany', $this->isCompany);

        if (!empty($aArguments)) {
            array_key_exists(
                'anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['titel'] : $aFormData['titel'] = 1;
            array_key_exists('name', $aArguments) ? $aFormData['name'] = $aArguments['name'] : $aFormData['name'] = "";
            array_key_exists(
                'vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'strasse',
                $aArguments
            ) ? $aFormData['strasse'] = $aArguments['strasse'] : $aFormData['strasse'] = "";
            array_key_exists('ort', $aArguments) ? $aFormData['ort'] = $aArguments['ort'] : $aFormData['ort'] = "";
            array_key_exists('plz', $aArguments) ? $aFormData['plz'] = $aArguments['plz'] : $aFormData['plz'] = "";
            array_key_exists(
                'geburtsdatum',
                $aArguments
            ) ? $aFormData['geburtsdatum'] = $aArguments['geburtsdatum'] : $aFormData['geburtsdatum'] = null;
            array_key_exists(
                'email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['email'] : $aFormData['email'] = "";
            array_key_exists(
                'telefon',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['telefon'] : $aFormData['telefon'] = "";
            array_key_exists(
                'mobile',
                $aArguments
            ) ? $aFormData['mobile'] = $aArguments['mobile'] : $aFormData['mobile'] = "";
            array_key_exists(
                'bank_accountowner',
                $aArguments
            ) ? $aFormData['bank_accountowner'] = $aArguments['bank_accountowner'] : $aFormData['bank_accountowner'] = "";
            array_key_exists(
                'kontotyp',
                $aArguments
            ) ? $aFormData['kontotyp'] = $aArguments['kontotyp'] : $aFormData['kontotyp'] = "SEPA";
            array_key_exists(
                'kontonummer',
                $aArguments
            ) ? $aFormData['kontonummer'] = $aArguments['kontonummer'] : $aFormData['kontonummer'] = "";
            array_key_exists('blz', $aArguments) ? $aFormData['blz'] = $aArguments['blz'] : $aFormData['blz'] = "";
            array_key_exists('iban', $aArguments) ? $aFormData['iban'] = $aArguments['iban'] : $aFormData['iban'] = "";
            array_key_exists('bank', $aArguments) ? $aFormData['bank'] = $aArguments['bank'] : $aFormData['bank'] = "";
            array_key_exists(
                'email_freigabe',
                $aArguments
            ) ? $aFormData['email_freigabe'] = $aArguments['email_freigabe'] : $aFormData['email_freigabe'] = 0;
            array_key_exists(
                'course_event_id',
                $aArguments
            ) ? $course_event_id = intval($aArguments['course_event_id']) : $course_event_id = 0;


            //if course_amount isset, then set as FormData['course_event_price'] - this payment amount includes the reductions
            array_key_exists(
                'course_amount',
                $aArguments
            ) ? $aFormData['course_event_price'] = $aArguments['course_amount'] : 0;

            //if course_reductions isset, then set as FormData
            array_key_exists(
                'course_reductions',
                $aArguments
            ) ? $aFormData['course_reductions'] = intval($aArguments['course_reductions']) : 0;

            //if course_waitinglist isset, then set as FormData
            array_key_exists(
                'course_waitinglist',
                $aArguments
            ) ? $aFormData['course_waitinglist'] = intval($aArguments['course_waitinglist']) : 0;



            // set number of resvertions: 1 Person
            $this->resvertions = 1;
        }

        // Cild form
        if ($this->isChild) {

            //get childs
            if (array_key_exists('sub', $aArguments) && is_array($aArguments['sub'])) {
                $i = 1;
                foreach ($aArguments['sub'] as $subscriber) {
                    // min set is name
                    if (!empty($subscriber['name'])) {
                        !empty($subscriber['anrede']) ? $aFormData['subscriber'][$i]['anrede'] = $subscriber['anrede'] : $aFormData['subscriber'][$i]['anrede'] = 1;
                        !empty($subscriber['vorname']) ? $aFormData['subscriber'][$i]['vorname'] = $subscriber['vorname'] : $aFormData['subscriber'][$i]['vorname'] = "";

                        if (!empty($subscriber['geburtsdatum'])) {
                            $aFormData['subscriber'][$i]['geburtsdatum'] = date(
                                'Y-m-d',
                                strtotime($subscriber['geburtsdatum'])
                            );
                        }

                        $aFormData['subscriber'][$i]['name'] = $subscriber['name'];

                        !empty($subscriber['phone']) ? $aFormData['subscriber'][$i]['phone'] = $subscriber['phone'] : $aFormData['subscriber'][$i]['phone'] = "";
                        !empty($subscriber['mobile']) ? $aFormData['subscriber'][$i]['mobile'] = $subscriber['mobile'] : $aFormData['subscriber'][$i]['mobile'] = "";
                        !empty($subscriber['email']) ? $aFormData['subscriber'][$i]['email'] = $subscriber['email'] : $aFormData['subscriber'][$i]['email'] = "";

                        $i++;

                    }
                }
            }

            // check if one child is mandatory
            if (boolval($this->settings['Required']['child']) &&
                (!isset($aFormData['subscriber']) || count($aFormData['subscriber']) == 0)) {
                $this->error = ['Error' => 'Mindestens ein Kind muss eingetragen werden.'];
                $errors = true;
            } else {
                // set number of reservations: only adults are counted, children are free
                $this->resvertions = 1;
            }
        }

        //if is company registration
        if ($this->isCompany) {

            array_key_exists(
                'firma',
                $aArguments
            ) ? $aFormData['firma'] = $aArguments['firma'] : $aFormData['firma'] = "";
            array_key_exists(
                'company_phone',
                $aArguments
            ) ? $aFormData['company_phone'] = $aArguments['company_phone'] : $aFormData['company_phone'] = "";
            array_key_exists(
                'company_email',
                $aArguments
            ) ? $aFormData['company_email'] = $aArguments['company_email'] : $aFormData['company_email'] = "";
            array_key_exists(
                'company_kontotyp',
                $aArguments
            ) ? $aFormData['kontotyp'] = $aArguments['company_kontotyp'] : $aFormData['kontotyp'] = "SEPA";

            array_key_exists(
                'contact_anrede',
                $aArguments
            ) ? $aFormData['anrede'] = $aArguments['contact_anrede'] : $aFormData['anrede'] = 1;
            array_key_exists(
                'contact_titel',
                $aArguments
            ) ? $aFormData['titel'] = $aArguments['contact_titel'] : $aFormData['titel'] = 1;
            array_key_exists(
                'contact_name',
                $aArguments
            ) ? $aFormData['name'] = $aArguments['contact_name'] : $aFormData['name'] = "";
            array_key_exists(
                'contact_vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['contact_vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'contact_phone',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['contact_phone'] : $aFormData['telefon'] = "";
            array_key_exists(
                'contact_mobile',
                $aArguments
            ) ? $aFormData['mobile'] = $aArguments['contact_mobile'] : $aFormData['mobile'] = "";
            array_key_exists(
                'contact_email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['contact_email'] : $aFormData['email'] = "";


            //get subscriber
            if (array_key_exists('sub', $aArguments) && is_array($aArguments['sub'])) {
                $i = 1;
                foreach ($aArguments['sub'] as $subscriber) {
                    // min set is name
                    if (!empty($subscriber['name'])) {
                        !empty($subscriber['anrede']) ? $aFormData['subscriber'][$i]['anrede'] = $subscriber['anrede'] : $aFormData['subscriber'][$i]['anrede'] = 1;
                        !empty($subscriber['vorname']) ? $aFormData['subscriber'][$i]['vorname'] = $subscriber['vorname'] : $aFormData['subscriber'][$i]['vorname'] = "";

                        if (!empty($subscriber['geburtsdatum'])) {
                            $aFormData['subscriber'][$i]['geburtsdatum'] = date(
                                'Y-m-d',
                                strtotime($subscriber['geburtsdatum'])
                            );
                        } elseif ((bool)$this->settings['showCompanyBirthdayField'] && (bool)$this->settings['requireCompanyBirthdayField']) {
                            $this->error = ['Error' => 'Das Geburtsdatum eines der Teilnehmer fehlt.'];
                            $errors = true;
                        }

                        $aFormData['subscriber'][$i]['name'] = $subscriber['name'];

                        !empty($subscriber['phone']) ? $aFormData['subscriber'][$i]['phone'] = $subscriber['phone'] : $aFormData['subscriber'][$i]['phone'] = "";
                        !empty($subscriber['mobile']) ? $aFormData['subscriber'][$i]['mobile'] = $subscriber['mobile'] : $aFormData['subscriber'][$i]['mobile'] = "";
                        !empty($subscriber['email']) ? $aFormData['subscriber'][$i]['email'] = $subscriber['email'] : $aFormData['subscriber'][$i]['email'] = "";
                        !empty($subscriber['strasse']) ? $aFormData['subscriber'][$i]['strasse'] = $subscriber['strasse'] : $aFormData['subscriber'][$i]['strasse'] = "";
                        !empty($subscriber['ort']) ? $aFormData['subscriber'][$i]['ort'] = $subscriber['ort'] : $aFormData['subscriber'][$i]['ort'] = "";
                        !empty($subscriber['plz']) ? $aFormData['subscriber'][$i]['plz'] = $subscriber['plz'] : $aFormData['subscriber'][$i]['plz'] = "";
                        $i++;
                    }
                }
            }

            //min one subscriber is needet
            if (!isset($aFormData['subscriber']) || count($aFormData['subscriber']) == 0) {
                $this->error = ['Error' => 'Mindesten ein Teilnehmer muss eingetragen werden.'];
                $errors = true;
            } else {
                // set number of resvertions: count of subscriber
                $this->resvertions = count($aFormData['subscriber']);
            }
        }

        // PayPal payment

        if ($this->paypal_is_active) {

            // check for authorized PayPal payment
            array_key_exists(
                'paypal_authorized',
                $aArguments
            ) && $aArguments['paypal_authorized'] == '1' ? $paypal_authorized = true : $paypal_authorized = false;

            if ($paypal_authorized) {
                $aFormData['paypal']['payer_id'] = array_key_exists(
                    'paypal_payer_id',
                    $aArguments
                ) ? $aArguments['paypal_payer_id'] : "";

                $aFormData['paypal']['invoice_number'] = array_key_exists(
                    'paypal_invoice_number',
                    $aArguments
                ) ? $aArguments['paypal_invoice_number'] : "";

                $aFormData['paypal']['order_id'] =
                $paypal_order_id =  array_key_exists(
                    'paypal_order_id',
                    $aArguments
                ) ? $aArguments['paypal_order_id'] : "";

                $aFormData['paypal']['auth_id'] =
                $paypal_auth_id =  array_key_exists(
                    'paypal_auth_id',
                    $aArguments
                ) ? $aArguments['paypal_auth_id'] : "";

                $this->view->assign('paypal_payment', $aFormData['paypal']);
            }
        }

        $this->view->assign('paypal_authorized', $paypal_authorized);


        //check term-check
        if (
            $this->validateUrl($this->settings['termsUrl'] ??= '') &&
            (!array_key_exists('terms_check', $aArguments) || intval($aArguments['terms_check']) != 1)
        ) {
            $this->error = ['Error' => 'Sie haben den AGB nicht zugestimmt.'];
            $errors = true;
        }

        // check course_event_id
        if (empty($course_event_id)) {
            $this->error = ['Error' => 'Es wurde keine Kursnummer übergeben!'];
            $errors = true;
        }

        $aSendObject[0] = $this->settings['apiKey'];
        $aSendObject[1] = $aFormData;
        $aSendObject[2] = $course_event_id;
        $aSendObject[3] = $this->isChild ? 'children' : ($this->isCompany ? 'company' : 'person');

        //check if we have an empty form
        $sCheck = $aArguments['name'] ??= '' . $aArguments['vorname'] ??= '' . $aArguments['firma'] ??= '' . $aArguments['bank_accountowner'] ??= '';
        if (empty($sCheck)) {
            $this->error = ['Error' => 'Bitte kein leeres Formular absenden!'];
            $errors = true;
        }

        // Special handling for requirePrivatBirthdayField
        // TODO at the moment, this field is not present!
        if ($this->settings['requirePrivatBirthdayField'] && $this->settings['showPrivatBirthdayField'] && array_key_exists(
                'geburtsdatum',
                $aArguments
            )) {
            array_push($this->mandatoryFormFields['person'], 'geburtsdatum');
        }

        // Add required fields from required settings
        // $plugin.tx_drkcourseregistration_courseregistrationform.settings.Required

        foreach ($this->settings['Required'] as $fieldname => $required) {
            if ($required) {
                if (strpos($fieldname, 'company_bg') !== false ||
                   (!$this->isChild && strpos($fieldname, 'child') !== false)
                )
                {
                    // ignore company_bg required or if not a child form
                    continue;
                } elseif (strpos($fieldname, 'company') !== false) {
                    array_push($this->mandatoryFormFields['company'], $fieldname);
                } elseif (strpos($fieldname, 'company') === false) {
                    array_push($this->mandatoryFormFields['person'], $fieldname);
                    array_push($this->mandatoryFormFields['company'], $fieldname);
                }
            }
        }

        if (!$this->isCompany) {
            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);

        } else {
            $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['company'], $aFormData);
        }

        //check if form send twice
        $aFormSessionInfo = $GLOBALS['TSFE']->fe_user->getKey('ses', 'CourseregistrationFbwForm');
        $iWaitTimeResendForm = is_numeric($this->settings['WaitTimeResendForm']) && $this->settings['WaitTimeResendForm'] > 0 ? $this->settings['WaitTimeResendForm'] : 600;
        if (is_array($aFormSessionInfo) && $aFormSessionInfo['time'] + $iWaitTimeResendForm >= time() && $aFormSessionInfo['hash'] == md5(serialize($aSendObject))) {
            $this->error = ['Error' => 'Das Formular bitte nicht mehrfach absenden!'];
            $errors = true;
        }

        // get fresh CourseData for last checks and ThanksPage
        $aParams[0] = $this->settings['apiKey'];
        $aParams[1] = $course_event_id;

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['birthname']))
        {
            $errors = true;
        }

        // workaround for PayPal payments - if PayPal is paypal_authorized, then don't check mandatoryFields
        if ($paypal_authorized) {
            $mandatoryFieldsFilled = true;
        }

        if (!$aCourseData = $this->courseregistrationFormRepository->getFbwCourseEvent($aParams)) {
            $this->error = ['Error' => 'Der Gebuchte Kurs wurde nicht mehr gefunden!'];
            $errors = true;
        }

        if (!$errors && $mandatoryFieldsFilled) {

            //check for possible overbooking
            if (intval($aCourseData['course_event_freeplaces']) < $this->resvertions) {
                $this->view->assign('bWaitinglist_active', true);
            } else {
                $this->view->assign('bWaitinglist_active', false);
            }

            // sending Data are successful
            if ($this->courseregistrationFormRepository->sendCustomerData($aSendObject,'FBW') ) {
                $this->view->assign('sending_ok', true);

                //set form fingerprint to session to block sending twice
                $aFormSessionInfo['hash'] = md5(serialize($aSendObject));
                $aFormSessionInfo['time'] = time();
                $GLOBALS['TSFE']->fe_user->setKey('ses', 'CourseregistrationFbwForm', $aFormSessionInfo);

                // capture PayPal payment
                if ($paypal_authorized) {
                    $paypal_payment_captured = $this->CapturePaypalPayment($paypal_order_id, $paypal_auth_id);
                }

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->settings['Debug']) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uri = $uriBuilder->build();
                    $this->redirectToUri($uri);
                    exit;
                } // else go on a render the template
                else {
                    unset($aSendObject);
                    $aSendObject[0] = $this->settings['apiKey'];
                    $aSendObject[1] = $course_event_id;

                    if ($aCourseData = $this->courseregistrationFormRepository->getFbwCourseEvent($aSendObject)) {
                        $this->view->assign('Error', false);
                    } else {
                        $this->view->assign('Error', true);
                        $this->error_reporting();
                    }

                    $aClientData = $aFormData;
                    $aClientData['anrede'] = array_key_exists(
                        $aFormData['anrede'],
                        \DRK\DrkGeneral\Utilities\Utility::$prefixArray
                    ) ? \DRK\DrkGeneral\Utilities\Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                    $aClientData['titel'] = array_key_exists(
                        $aFormData['titel'],
                        \DRK\DrkGeneral\Utilities\Utility::$titleArray
                    ) ? \DRK\DrkGeneral\Utilities\Utility::$titleArray[$aFormData['titel']] : "";

                    $this->view->assign('aCourseData', $aCourseData);
                    $this->view->assign('aClientData', $aClientData);
                    $this->view->assign('isCompany', $this->isCompany);
                    $this->view->assign('sWaitinglist_message', $this->settings['waitinglistMessage']);

                    // set bPayPalSuccess for captured payment
                    $this->view->assign('bPayPalSuccess', $paypal_payment_captured);
                }
            }
        }

        // if Error, then show it now
        if (!empty($this->error)) {
            $this->view->assign('error', true);
            $this->error_reporting();

            // Cancel PayPal order on error
            $this->CancelOrder($paypal_auth_id);
        } else {
            $this->view->assign('error', false);
        }

        return $this->htmlResponse();
    }

    /**
     * Init
     *
     * @return void
     * @throws InvalidFileException
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addJavaScript(
            'courseregistration-fbw-js',
            'EXT:drk_courseregistration/Resources/Public/Scripts/tx_drkcourseregistrationform_fbw.js',
            [],
            ['priority' => false] // lädt in footer
        );
    }

}
