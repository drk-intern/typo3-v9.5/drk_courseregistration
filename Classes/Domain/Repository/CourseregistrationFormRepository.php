<?php

namespace DRK\DrkCourseregistration\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;

/**
 * Class CourseregistrationFormRepository
 * @package DRK\DrkCourseregistration\Domain\Repository
 */
class CourseregistrationFormRepository extends AbstractDrkRepository
{

    /**
     * @param array $request
     *
     * @return array|bool
     */
    public function getCourseEvent(array $request = [])
    {
        try {

            return $this->executeJsonClientAction('getCourseEvent', $request);

        } catch (Exception $exception) {

            $this->error = ['Fehler' => $exception->getMessage()];
            return false;

        }
    }

    /**
     * @param array $request
     *
     * @return array|bool
     * @throws \Exception
     */
    public function getFbwCourseEvent(array $request = [])
    {
        try {

            return $this->executeJsonClientAction('getFbwCourseEvent', $request);

        } catch (Exception $exception) {

            $this->error = ['Fehler' => $exception->getMessage()];
            return false;

        }

    }

    /**
     * @param array $request
     *
     * @return array|bool
     * @throws \Exception
     */
    public function getLsCourseEvent(array $request = [])
    {
        try {

            return $this->executeJsonClientAction('getLsCourseEvent', $request);

        } catch (Exception $exception) {

            $this->error = ['Fehler' => $exception->getMessage()];
            return false;

        }
    }

    /**
     *
     * sendCustomerData
     *
     * @param array $aSendObject
     * @param string $sFormTyp
     * @return bool|array
     * @throws \Exception
     */
    public function sendCustomerData(array $aSendObject = [], string $sFormTyp = ''): bool|array
    {
        // On debug, don't send data
        if ($this->settings['Debug']) {
            \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($aSendObject);
            return true;
        }

        if (empty($aSendObject)) {
            $this->error = ['Error' => 'No object to send!'];
            return false;
        }

        // sort SendObject for the right parameter position
        ksort($aSendObject);

        $client = $this->getJsonClient();

        switch ($sFormTyp) {
            case 'LS':
                $response = $client->execute('sendLsRegistration', $aSendObject);
                break;
            case 'FBW':
                $response = $client->execute('sendFbwRegistration', $aSendObject);
                break;
            default:
                $response = $client->execute('sendRegistration', $aSendObject);
        }

        //catch Service Error
        if (!empty($response)) {
            if ($response['status'] != "OK") {

                // translate error messages
                switch ($response['message']) {
                    case "IBAN is not valid":
                        $this->error = ['Error' => "Leider wurde ein Fehler in Ihrer IBAN oder der Kontoverbindung festgestellt!"];
                        break;
                    case "Overbooking":
                        $this->error = ['Warnung' => "Achtung, der Kurs wurde überbucht. Eine Verschiebung auf einen anderen Kurstermin ist wahrscheinlich."];
                        break;
                    case "Room Reservation":
                        $this->error = ['Warnung' => "Achtung, es konnten nicht mehr alle Übernachtungen reserviert werden!"];
                        break;
                    case "Courseregistration already exists!":
                        $this->error = ['Warnung' => "Achtung, Ihre Anmeldung wurde bereite entgegen genommen!"];
                        break;
                    default:
                        $this->error = ['Error' => "Der Webservice meldet: " . $response['message']];
                }

                // only an error send false
                if (array_key_exists("Error", $this->error)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            $this->error = ['Error' => 'Webservice antwortet nicht oder fehlerhaft!'];
            return false;
        }
    }
}
