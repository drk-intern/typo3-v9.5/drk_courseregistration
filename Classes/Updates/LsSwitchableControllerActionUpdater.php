<?php
namespace DRK\DrkCourseregistration\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_courseregistration_ls-SwitchableControlleractionUpdater')]
class LsSwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkcourseregistration_courseregistrationformls',
            'switchableControllerActions' => 'CourseregistrationFormLs->showSingleForm;CourseregistrationFormLs->send',
            'targetListType' => '',
            'targetCtype' => 'dkcourseregistration_courseregistrationlssingleform'
        ], [
            'sourceListType' => 'drkcourseregistration_courseregistrationformls',
            'switchableControllerActions' => 'CourseregistrationFormLs->showCompanyForm;CourseregistrationFormLs->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationlscompanyform'
        ],
    ];
}
