<?php
declare(strict_types=1);

namespace DRK\DrkCourseregistration\Updates;

/**
 * This file is part of the "news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Doctrine\DBAL\FetchMode;
use InvalidArgumentException;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Updates list type for CourseregistrationFormLs
 */
#[UpgradeWizard('drkcourseregistrationPluginEntries')]

class UpdatePluginEntries extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected $table = 'tt_content';

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return 'Adjust tt_content records for plugin drkcourseregistration';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'Adjust tt_content records for plugin drkcourseregistration';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkcourseregistrationls_courseregistrationlsform%"')
            )
            ->execute()
            ->fetchAllAssociative();

        foreach ($results as $result) {
            $result['list_type'] = str_replace(
                'drkcourseregistrationls_courseregistrationlsform',
                'drkcourseregistration_courseregistrationformls',
                $result['list_type']
            );

            $connection->update(
                $this->table,
                [
                    'list_type' => $result['list_type'],
                    'pi_flexform' => $result['pi_flexform']
                ],
                [
                    'uid' => $result['uid']
                ]
            );
        }
        return true;
    }

    /**
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkcourseregistrationls_courseregistrationlsform%"')
            )
            ->execute()
            ->fetchAllNumeric();
        return $numberOfEntries > 0;
    }
}
