<?php
namespace DRK\DrkCourseregistration\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_courseregistration-SwitchableControlleractionUpdater')]
class SwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkcourseregistration_courseregistrationform',
            'switchableControllerActions' => 'CourseregistrationForm->showCourseregistrationForm;CourseregistrationForm->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationgeneralsingleform'
        ], [
            'sourceListType' => 'drkcourseregistration_courseregistrationform',
            'switchableControllerActions' => 'CourseregistrationForm->showCompanyFullCourseregistrationForm;CourseregistrationForm->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationgeneralfullform'
        ], [
            'sourceListType' => 'drkcourseregistration_courseregistrationform',
            'switchableControllerActions' => 'CourseregistrationForm->showCompanySelfpayerCourseregistrationForm;CourseregistrationForm->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationgeneralcompanyform'
        ],
    ];
}
