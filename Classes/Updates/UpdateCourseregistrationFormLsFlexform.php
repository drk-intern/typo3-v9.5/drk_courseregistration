<?php
declare(strict_types=1);

namespace DRK\DrkCourseregistration\Updates;

/**
 * This file is part of the "drkcourseregistration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Doctrine\DBAL\FetchMode;
use InvalidArgumentException;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Update Flexform to new default
 */
#[UpgradeWizard('drkcourseregistrationCourseregistrationFormLsFlexform')]

class UpdateCourseregistrationFormLsFlexform extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected $table = 'tt_content';

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return 'Adjust flexform for plugin drkcourseregistration ls';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'Adjust flexform for plugin drkcourseregistration ls. After adding the single form, the company form must be set as default.';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkcourseregistration_courseregistrationformls%"')
            )
            ->execute()
            ->fetchAllAssociative();

        foreach ($results as $result) {

            $connection->update(
                $this->table,
                [
                    'pi_flexform' => '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<T3FlexForms>
    <data>
        <sheet index="general">
            <language index="lDEF">
                <field index="settings.defaultform">
                    <value index="vDEF">0</value>
                </field>
                <field index="switchableControllerActions">
                    <value index="vDEF">CourseregistrationFormLs-&gt;showCompanyForm;CourseregistrationFormLs-&gt;send</value>
                </field>
            </language>
        </sheet>
    </data>
</T3FlexForms>'
                ],
                [
                    'uid' => $result['uid']
                ]
            );
        }
        return true;
    }

    /**
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->like('list_type', '"%drkcourseregistration_courseregistrationformls%"')
            )
            ->execute()
            ->fetchAllNumeric();
        return $numberOfEntries > 0;
    }
}
