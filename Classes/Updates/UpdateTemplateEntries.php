<?php
declare(strict_types=1);

namespace DRK\DrkCourseregistration\Updates;

/**
 * This file is part of the "drk_courseregistration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Fills sys_category.slug with a proper value
 */
#[UpgradeWizard('drkcourseregistrationupdateTemplateEntries')]
class UpdateTemplateEntries extends AbstractRecordUpdater implements UpgradeWizardInterface
{
    protected $table = 'sys_template';

    /**
     * @return string Title of this updater
     */
    public function getTitle(): string
    {
        return '[drk_template]: Adjust sys_template records for new plugin name (drk_courseregistration)';
    }

    /**
     * @return string Longer description of this updater
     */
    public function getDescription(): string
    {
        return 'Adjust sys_template records for new plugin name';
    }

    /**
     * Performs the accordant updates.
     *
     * @return bool Whether everything went smoothly or not
     * @throws Exception
     */
    public function executeUpdate(): bool
    {
        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->table);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $results = $queryBuilder
            ->select('*')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->or(
                    $queryBuilder->expr()->like('config', '"%plugin.tx_drkcourseregistration_courseregistrationformfbw%"'),
                    $queryBuilder->expr()->like('config', '"%plugin.tx_drkcourseregistration_courseregistrationformls%"')
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();

        foreach ($results as $result) {
            $result['config'] = str_replace(
                'plugin.tx_drkcourseregistration_courseregistrationformfbw',
                'plugin.tx_drkcourseregistration',
                $result['config']
            );
            $result['config'] = str_replace(
                'plugin.tx_drkcourseregistration_courseregistrationformls',
                'plugin.tx_drkcourseregistration',
                $result['config']
            );
            $connection->update(
                $this->table,
                [
                    'config' => $result['config']
                ],
                [
                    'uid' => $result['uid']
                ]
            );
        }
        return true;
    }

    /**
     * Check if there are record within database table with an empty "slug" field.
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    protected function checkIfWizardIsRequired(): bool
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from($this->table)
            ->where(
                $queryBuilder->expr()->or(
                    $queryBuilder->expr()->like('config', '"%plugin.tx_drkcourseregistration_courseregistrationformfbw%"'),
                    $queryBuilder->expr()->like('config', '"%plugin.tx_drkcourseregistration_courseregistrationformls%"')
                )
            )
            ->executeQuery()
            ->fetchOne();
        return $numberOfEntries > 0;
    }
}
