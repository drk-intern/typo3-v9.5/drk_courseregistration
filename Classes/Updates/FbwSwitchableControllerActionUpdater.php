<?php
namespace DRK\DrkCourseregistration\Updates;

use DRK\DrkGeneral\Updates\AbstractSwitchableControllerActionsPluginUpdater;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drk_courseregistration_fbw-SwitchableControlleractionUpdater')]
class FbwSwitchableControllerActionUpdater extends AbstractSwitchableControllerActionsPluginUpdater
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'drkcourseregistration_courseregistrationformfbw',
            'switchableControllerActions' => 'CourseregistrationFormFbw->showChildrenForm;CourseregistrationFormFbw->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationfbwchildrenform'
        ], [
            'sourceListType' => 'drkcourseregistration_courseregistrationformfbw',
            'switchableControllerActions' => 'CourseregistrationFormFbw->showSingleForm;CourseregistrationFormFbw->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationfbwsingleform'
        ], [
            'sourceListType' => 'drkcourseregistration_courseregistrationformfbw',
            'switchableControllerActions' => 'CourseregistrationFormFbw->showCompanyForm;CourseregistrationFormFbw->send',
            'targetListType' => '',
            'targetCtype' => 'drkcourseregistration_courseregistrationfbwcompanyform'
        ],
    ];
}
