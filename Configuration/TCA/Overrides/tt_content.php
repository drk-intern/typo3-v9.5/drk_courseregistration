<?php

######################################
###  CourseregistrationGeneralForm ###
######################################

## CourseregistrationGeneralSingleForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralSingleForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationgeneralsingleform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

## CourseregistrationGeneralFullForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralFullForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationgeneralfullform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseregistration/Configuration/FlexForms/courseregistration.xml',
    // ctype
    'drkcourseregistration_courseregistrationgeneralfullform'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellung,
            pi_flexform'
    ]
);

## CourseregistrationGeneralCompanyForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralCompanyForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationgeneralcompanyform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

###################################
###  FBW CourseregistrationForm ###
###################################

## CourseregistrationFbwChildrenForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationFbwChildrenForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationfbwchildrenform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

## CourseregistrationFbwSingleForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationFbwSingleForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationfbwsingleform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

## CourseregistrationFbwCompanyForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationFbwCompanyForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationfbwcompanyform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseregistration/Configuration/FlexForms/courseregistration-fbw.xml',
    // ctype
    'drkcourseregistration_courseregistrationfbwcompanyform'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellung,
            pi_flexform'
    ]
);

###################################
###  LS CourseregistrationForm ###
###################################

## CourseregistrationLsSingleForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationLsSingleForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationlssingleform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);

## CourseregistrationLsCompanyForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationLsCompanyForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationlscompanyform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_courseregistration/Configuration/FlexForms/courseregistration-ls.xml',
    // ctype
    'drkcourseregistration_courseregistrationlscompanyform'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellung,
            pi_flexform'
    ]
);

## CourseregistrationLsDogForm Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_courseregistration',
    'CourseregistrationLsDogForm',
    'LLL:EXT:drk_courseregistration/Resources/Private/Language/locallang_be.xlf:tt_content.courseregistrationlsdogform_plugin.name',
    'EXT:drk_courseregistration/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kursanmeldung'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
