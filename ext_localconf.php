<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}
######################################
###  CourseregistrationGeneralForm ###
######################################

## CourseregistrationGeneralSingleForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralSingleForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showSingleForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showSingleForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationGeneralFullForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralFullForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showFullForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showFullForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationGeneralCompanyForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationGeneralCompanyForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showCompanyForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormController::class => 'showCompanyForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);


###################################
###  FBW CourseregistrationForm ###
###################################

## CourseregistrationFbwChildrenForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationFbwChildrenForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showChildrenForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showChildrenForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationFbwSingleForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationFbwSingleForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showSingleForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showSingleForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationFbwCompanyForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationFbwCompanyForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showCompanyForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormFbwController::class => 'showCompanyForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

###################################
###  LS CourseregistrationForm ###
###################################

## CourseregistrationLsSingleForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationLsSingleForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showSingleForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showSingleForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationLsCompanyForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationLsCompanyForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showCompanyForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showCompanyForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

## CourseregistrationLsDogForm Plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_courseregistration',
    'CourseregistrationLsDogForm',
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showDogForm,send',

    ],
    // non-cacheable actions
    [
        \DRK\DrkCourseregistration\Controller\CourseregistrationFormLsController::class => 'showDogForm,send',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
