// special functions for fbw-form
$(function () {
  'use strict';

  let form = $('#courseregistrationform');

  // set course amount if course_reductions isset
  if ($('#courseregistrationform #course_reductions').length > 0) {

    //init
    set_amount();

    $('#courseregistrationform #course_reductions').change(function () {
      set_amount();
    });

  }

  // if requirePrivatBirthday isset, set birthday required on demand
  if ($('#courseregistrationform [name*=requirePrivatBirthday]').length > 0) {
    $('#courseregistrationform .children [id^=sub]').change(function () {
      if ($(this).val().length > 0) {
        $(this).parent('div').parent('div').find('[id$=_geburtsdatum]').prop('required', true);
      } else {
        $(this).parent('div').parent('div').find('[id$=_geburtsdatum]').prop('required', false);
      }
    });
  }

  // Paypal
  if ($('#paypal-payment').length) {
    $('#paypal-payment').show();

    if ($('#paypal_only').val() == 1) {
      $('.tx-drk-courseregistrationform-submit').hide();
    }
  }

  $('#courseregistrationform.multiform input[type=radio].check').change(function () {
      if ($('input#company').is(':checked')) {
        $('.tx-drk-courseregistrationform-person').hide();
        $('.tx-drk-courseregistrationform-company').show();
        $('.tx-drk-courseregistrationform-person input, .tx-drk-courseregistrationform-person select, .tx-drk-courseregistrationform-person textarea').prop('disabled', true);
        $('.tx-drk-courseregistrationform-company input, .tx-drk-courseregistrationform-company select, .tx-drk-courseregistrationform-company textarea').prop('disabled', false);
      } else {
        $('.tx-drk-courseregistrationform-company').hide();
        $('.tx-drk-courseregistrationform-person').show();
        $('.tx-drk-courseregistrationform-person input, .tx-drk-courseregistrationform-person select, .tx-drk-courseregistrationform-person textarea').prop('disabled', false);
        $('.tx-drk-courseregistrationform-company input, .tx-drk-courseregistrationform-company select, .tx-drk-courseregistrationform-company textarea').prop('disabled', true);
      }
  });
});

/**
 * set course amount by course_reductions
 */
function set_amount() {
  var amount = parseInt($('#courseregistrationform #course_event_price').val());
  var course_reduction_id = 0;
  var course_reduction = [];

  if ($('#courseregistrationform #course_reductions').length > 0) {
    course_reduction_id = parseInt($('#courseregistrationform #course_reductions').val());

    course_reduction = $.parseJSON($('#courseregistrationform #course_reduction_' + course_reduction_id).val());

    if (course_reduction['value_type'] == 'percent') {
      //console.log('percent');

      if ( parseInt(course_reduction['value']) < 100 ) {
        amount = ($('#courseregistrationform #course_event_price').val() - ((course_reduction['value'] / 100) *
          $('#courseregistrationform #course_event_price').val()));
      }
      else {
        amount = parseInt($('#courseregistrationform #course_event_price').val());
      }

    } else {
      // if the course_reducion is greater than the course_event_price, then ignore the course_reducion
      if ( ($('#courseregistrationform #course_event_price').val() - course_reduction['value']) >= 1 ) {
        amount = ($('#courseregistrationform #course_event_price').val() - course_reduction['value']
          )
        ;
      }
      else {
        amount = parseInt($('#courseregistrationform #course_event_price').val());
      }
    }

    //console.log('amount');

    if ($('#courseregistrationform #paypal_amount').length > 0) {
      $('#courseregistrationform #paypal_amount').val(amount.toFixed(2));
    }
    $('#courseregistrationform #course_amount').val(amount.toFixed(2));
    $('#courseregistrationform .course_event_price').text(amount.toLocaleString('de-DE', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 2
    }));
  }
}
