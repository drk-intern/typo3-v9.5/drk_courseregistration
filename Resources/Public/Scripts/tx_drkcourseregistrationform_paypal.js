// PayPal integration

jQuery.loadScript = function (url, callback) {
  jQuery.ajax({
    url: url,
    dataType: 'script',
    success: callback,
    async: true,
    cache: true
  });
};

if (typeof paypal == 'undefined') $.loadScript( paypal_src, function () {

  'use strict';

  console.log('PayPal');
  // PayPal integration
  let paypal_amount = '';
  let paypal_coursename = '';
  let paypal_coursenumber = '';
  let paypal_invoice_number = '';
  let paypal_description = '';
  let paypal_service_desc = '';
  let is_valid = false;
  const debug = false;
  const form = $('#courseregistrationform');


  //init
  paypal_amount = $('#paypal_amount').val();
  paypal_coursename = $('#paypal_coursename').val();
  paypal_coursenumber = $('#paypal_coursenumber').val();
  paypal_service_desc = $('#paypal_service_desc').val();
  paypal_description = 'Kursanmelung '+paypal_service_desc;

  function doValidation() {

    if (form[0].checkValidity()) {
      is_valid = true;
    } else {
      is_valid = false;
    }
  }

  function toggleButton(actions) {
    return is_valid ? actions.enable() : actions.disable();
  }

  function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  if ($('#courseregistrationform #paypal-button-container').length > 0) {
    paypal.Buttons({
      locale: 'de_DE',

      style: {
        shape: 'rect',
        color: 'blue',
        layout: 'vertical',
        label: 'paypal',

      },

      commit: true,

      // onInit is called when the button first renders
      onInit: function (data, actions) {

        // Disable the buttons if form is invalid
        if (!form[0].checkValidity()) {
          actions.disable();
        }
        else {
          is_valid = true;
        }

        $('#courseregistrationform input[type=\'text\'], #courseregistrationform input[type=\'checkbox\']').change(function () {
          doValidation();
          toggleButton(actions);
        });
      },

      createOrder: function (data, actions) {
        return actions.order.create({
          purchase_units: [
            {
              description: paypal_description,
              invoice_id: paypal_invoice_number,
              amount:
              {
                currency_code: 'EUR',
                value: paypal_amount,
                breakdown: {
                  item_total: {
                    currency_code: 'EUR',
                    value: paypal_amount,
                  }
                }
              },
              items: [{
                name: paypal_service_desc,
                description: paypal_coursename,
                sku: paypal_coursenumber,
                unit_amount: {
                  currency_code: 'EUR',
                  value: paypal_amount,
                },
                quantity: 1,
              }],
            }]
        });
      },

      onClick: function () {
        if (typeof form.parsley().validate() !== 'undefined') {
          form.parsley().validate();
        } else {
          form[0].reportValidity();
        }

        doValidation();

        // reset paypal_amount if changed
        paypal_amount = $('#paypal_amount').val();
        paypal_invoice_number = paypal_coursenumber + '/' + makeid(8);

        //clear honeypot - paypal payer are no spambot
        if ($('input[name^="birthname"]').length > 0) {
          $('input[name^="birthname"]').val = '';
        }
      },

      onApprove: function(data, actions) {
        // Authorize the transaction
        actions.order.authorize().then(function(authorization) {

          // Get the authorization id
          let authorizationID = authorization.purchase_units[0].payments.authorizations[0].id;

          // debug
          if (debug) {
            console.log('Order ID = ', data.orderID);
            console.log('Payer ID = ', authorization.payer.payer_id);
            console.log('Authorization ID = ', authorizationID);
            console.log('InvoiceNumber = ', paypal_invoice_number);
            console.log('Capture result', authorization, JSON.stringify(authorization, null, 2));
          }

          $('#paypal_order_id').val(data.orderID);
          $('#paypal_auth_id').val(authorizationID);
          $('#paypal_invoice_number').val(paypal_invoice_number);
          $('#paypal_authorized').val('1');
          $('#paypal_payer_id').val(authorization.payer.payer_id);

          if (authorization.status == 'COMPLETED') {
            // set paypal_authorized true
            $('#paypal_authorized').val('1');

            //show modal
            $('.tx-drk-courseregistrationform-send-modal').modal({
              closeExisting: false,    // Close existing modals. Set this to false if you need to stack multiple modal instances.
              escapeClose: false,      // Allows the user to close the modal by pressing `ESC`
              clickClose: false,       // Allows the user to close the modal by clicking the overlay
              showClose: false,        // Shows a (X) icon/link in the top-right corner
            });

            // Now submit the form
            form.submit();
          } else {
            $('#paypal_authorized').val('0');
            alert ('Ihre Paypal-Zahlung '+data.orderID+' war nicht erfolgreich! - Die Kursanmeldung konnte leider nicht durchgeführt werden.');
          }
        });
      },

      onCancel: function (data) {
        console.log('The payment was cancelled!');
        console.log('Order ID = ', data.id);

        // set paypal_authorized false
        $('#paypal_authorized').val('0');

      },

      onError: function (err) {
        console.log('Error!');
        console.log(err);

        alert('Ein Problem mit Ihrer PayPal-Zahlung ist aufgetreten.');

        // set paypal_authorized false
        $('#paypal_authorized').val('0');
      }

    }).render('#paypal-button-container');
  }
});
