/**
 *
 * DRK Courseregistration Javascript
 *
 */

$(function () {
  'use strict';

  let form = $('#courseregistrationform');
  setTimeout(
    function () {

      //trigger toggleFirmaCourseRegistration on load
      toggleFirmaCourseRegistration();
    }, 100);

  /**
   * Prevent users from submitting the form by hitting Enter
   */
  form.on('keydown', ':input:not(textarea)', function(event) {
    return event.key != 'Enter';
  });

  /**
   * Show modal on submit
   */
  form.on('submit', function(e){
    $('.tx-drk-courseregistrationform-send-modal').modal( {
      closeExisting: false,    // Close existing modals. Set this to false if you need to stack multiple modal instances.
      escapeClose: false,      // Allows the user to close the modal by pressing `ESC`
      clickClose: false,       // Allows the user to close the modal by clicking the overlay
      showClose: false,        // Shows a (X) icon/link in the top-right corner
    });
  });

  // if requireCompanyBirthdayField isset, set birthday required for subscriber on demand
  if ($('#courseregistrationform #requireCompanyBirthdayField').length > 0) {
    $('#courseregistrationform .subscriber [id^=sub]').change(function () {
      if ($(this).val().length > 0) {
        $(this).parent('div').parent('div').find('[id*=_geburtsdatum_]').prop('required', true);
      }
      else {
        $(this).parent('div').parent('div').find('[id*=_geburtsdatum_]').prop('required', false);
      }
    });
  }

  // if requireCompanyMailField isset, set mail required for subscriber on demand
  if ($('#courseregistrationform #requireCompanyMailField').length > 0) {
    $('#courseregistrationform .subscriber [id^=sub]').change(function () {
      if ($(this).val().length > 0) {
        $(this).parent('div').parent('div').find('[id*=_email]').prop('required', true);
      }
      else {
        $(this).parent('div').parent('div').find('[id*=_email]').prop('required', false);
      }
    });
  }

  let iban = $('#courseregistrationform input.iban');
  if (iban.length > 0) {
    setTimeout(
      function () {
        iban
          .mask('aa99 9999 9999 9999 9999 99', {
            placeholder: 'DE__ ____ ____ ____ ____ __'
          })
          .focusout(function () {
            $(this).attr('placeholder', '');
            if (this.value === 'DE__ ____ ____ ____ ____ __') {
              $(this).val('').removeClass('has-value');
            }
          });
      }, 100);
  }

  $('.error').removeClass('error');

});

/**
 * useKontonr
 */
function useKontonr(isCompany='') {
  $('#klassische_kontonr' + isCompany).show();
  $('#sepa_kontonr' + isCompany).hide();
}

/**
 * useSEPA
 */
function useSEPA(isCompany='') {
  $('#klassische_kontonr' + isCompany).hide();
  $('#sepa_kontonr' + isCompany).show();
}

function toggleFirmaCourseRegistration() {

  // just switch only in mutliform, not needed for singleform
  if ($('#courseregistrationform').hasClass('multiform')) {
    if ($('input#company').is(':checked')) {
      $('.tx-drk-courseregistrationform-person').hide();
      $('.tx-drk-courseregistrationform-companybg').hide();
      $('.tx-drk-courseregistrationform-company').show();
      $('.tx-drk-courseregistrationform-person input, .tx-drk-courseregistrationform-person select, .tx-drk-courseregistrationform-person textarea, #paypal-payment input').prop('disabled', true);
      $('.tx-drk-courseregistrationform-companybg input, .tx-drk-courseregistrationform-companybg select').prop('disabled', true);
      $('.tx-drk-courseregistrationform-company input, .tx-drk-courseregistrationform-company select, .tx-drk-courseregistrationform-company textarea').prop('disabled', false);
      $('#paypal-payment').hide();
      $('.tx-drk-courseregistrationform-submit').show();
    } else if ($('input#companybg').is(':checked')) {
      $('.tx-drk-courseregistrationform-person').hide();
      $('.tx-drk-courseregistrationform-company').hide();
      $('.tx-drk-courseregistrationform-companybg').show();
      $('.tx-drk-courseregistrationform-person input, .tx-drk-courseregistrationform-person select, .tx-drk-courseregistrationform-person textarea, #paypal-payment input').prop('disabled', true);
      $('.tx-drk-courseregistrationform-companybg input, .tx-drk-courseregistrationform-companybg select').prop('disabled', false);
      $('.tx-drk-courseregistrationform-company input, .tx-drk-courseregistrationform-company select, .tx-drk-courseregistrationform-company textarea').prop('disabled', true);
      $('#paypal-payment').hide();
      $('.tx-drk-courseregistrationform-submit').show();

    } else {
      $('.tx-drk-courseregistrationform-company').hide();
      $('.tx-drk-courseregistrationform-companybg').hide();
      $('.tx-drk-courseregistrationform-person').show();
      $('.tx-drk-courseregistrationform-person input, .tx-drk-courseregistrationform-person select, .tx-drk-courseregistrationform-person textarea, #paypal-payment input').prop('disabled', false);
      $('.tx-drk-courseregistrationform-companybg input, .tx-drk-courseregistrationform-companybg select').prop('disabled', true);
      $('.tx-drk-courseregistrationform-company input, .tx-drk-courseregistrationform-company select, .tx-drk-courseregistrationform-company textarea').prop('disabled', true);

      if ($('#paypal-payment').length) {
        $('#paypal-payment').show();

        if ($('#paypal_only').val() == 1) {
          $('.tx-drk-courseregistrationform-submit').hide();
        }
      }
    }
  }
}

