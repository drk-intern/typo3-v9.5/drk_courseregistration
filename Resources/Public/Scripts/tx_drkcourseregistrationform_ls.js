// special functions for ls-form

$(function () {
  'use strict';
  let aFields = ['org_name', 'org_street', 'org_zip', 'org_city'];

  /** set on start */
  if ($('#courseregistrationform #member').length > 0) {
    change_member_fields();
  }

  /** set on change */
  $('#courseregistrationform #member').change( function() {
    change_member_fields();
  });

  function change_member_fields()
  {
    if ($('#courseregistrationform #member').val() == 1) {

      aFields.forEach(function(sField) {
        $('#courseregistrationform #' + sField + '').prop('required', true);
        $('#courseregistrationform #' + sField + '').removeClass('error');
        $('#courseregistrationform label[for="' + sField + '"] > .must').show();
        $('#courseregistrationform #'+ sField + '-error').hide();
      });
    }
    else {
      aFields.forEach(function(sField) {
        $('#courseregistrationform #' + sField + '').prop('required', false);
        $('#courseregistrationform #' + sField + '').removeClass('error');
        $('#courseregistrationform label[for="' + sField + '"] > .must').hide();
        $('#courseregistrationform #'+ sField + '-error').hide();
      });
    }
  }
});
